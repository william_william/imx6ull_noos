int delay(int ndelay)
{
	volatile int n = ndelay;
	while(n--);
	
	return 0;
}

int led_init(void)
{
	volatile unsigned int *pReg;
	
	/* 1、设置 GPIO1_IO03 复用为 GPIO1_IO03 */
	pReg = (unsigned int *)(0x020e0000 + 0x68);
	*pReg |= (0x05);
	
	/* 2、设置GPIO1_IO03 为输出引脚 */
	pReg = (unsigned int *)(0x0209c000 + 0x04);
	*pReg |= (1<<3);
}

void led_on(void)
{
    volatile unsigned int *pReg;
    pReg = (unsigned int *)(0x0209c000);
    *pReg &= ~(1<<3);	/* 设置GPIO1_IO03输出0 */
}

void led_off(void)
{
    volatile unsigned int *pReg;
    pReg = (unsigned int *)(0x0209c000);
	*pReg |= (1<<3);    /* 设置GPIO1_IO03输出1 */
}
