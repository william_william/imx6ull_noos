#ifndef __KEY_H
#define __KEY_H

/** GPIO - Register Layout Typedef */
typedef struct {
  volatile unsigned int DR;                                /**< GPIO data register, offset: 0x0 */
  volatile unsigned int GDIR;                              /**< GPIO direction register, offset: 0x4 */
  volatile unsigned int PSR;                               /**< GPIO pad status register, offset: 0x8 */
  volatile unsigned int ICR1;                              /**< GPIO interrupt configuration register1, offset: 0xC */
  volatile unsigned int ICR2;                              /**< GPIO interrupt configuration register2, offset: 0x10 */
  volatile unsigned int IMR;                               /**< GPIO interrupt mask register, offset: 0x14 */
  volatile unsigned int ISR;                               /**< GPIO interrupt status register, offset: 0x18 */
  volatile unsigned int EDGE_SEL;                          /**< GPIO edge select register, offset: 0x1C */
} GPIO_Type;

void key_init(void);
void irq_hander_gpio1_16_32(void);

#endif
