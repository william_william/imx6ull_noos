
.text                               /* .text段保存代码，是只读和可执行的，后面的指令都属于.text段。 */
.global  _start                     /* .global表示_start是一个全局符号，会在链接器链接时用到 */

_start:                             /* 标签_start，汇编程序的默认入口是_start */
	b	reset						/* 复位异常，复位后从这里开始执行 */		
	ldr	pc,=_undefined_instruction  /* 未定义的指令异常，当cpu执行到不认识的指令时，会跳转到这里 */
	ldr	pc,=_software_interrupt     /* 软中断异常，当cpu执行svc指令时，会跳转到这里  */
	.word 0 /* ldr	pc,=_prefetch_abort         /* 预取中止异常，处理器预取指令的地址不存在，或不允许当前访问时 */
	.word 0 /* ldr	pc,=_data_abort             /* 数据中止异常，处理器数据访问指令的地址不存在，或不允许当前访问时 */
	.word 0 /* ldr	pc,=_not_used               /* 未使用，预留的地址 */
	ldr	pc,=_irq                    /* 中断请求有效，且CPSR中的I位(即中断屏蔽位)为0时，产生IRQ异常 */
	.word 0 /* ldr	pc,=_fiq                    /* 快速中断请求有效，且CPSR中的F位(即快中断屏蔽位)为0时，产生FIQ异常 */
reset:
    /* 1、设置栈 */
    ldr sp, =(0x80000000+0x100000)  /* 设置栈顶地址 */

    /* 2、设置异常向量表基地址 : VBAR */
	ldr r0, =_start
	mcr p15, 0, r0, c12, c0, 0

    /* 3、清除bss段 */
    ldr r1, =__bss_start            /* 将bss段开始地址存入r1寄存器 */
    ldr r2, =__bss_end              /* 将bss段结束地址存入r2寄存器 */
    b clean_bss
clean:
    mov r3, #0                      /* 将0存入r3寄存器 */
    str r3, [r1], #4                /* 将r3中的值存到r1中的值所指向的地址中, 同时r1中的值加4 */
clean_bss:
    cmp r1, r2                      /* 比较r1和r2内的值是否相等 */
    bne clean                       /* 如果不相等则跳转到clean标签处，如果相等则往下执行 */

    /* 4、调用系统初始化函数，在异常处理函数中需要用到uart功能，需要先初始化uart */
    bl SystemInit

    /* 5、获取当前CPSR的值存入r1中，r1会作为C函数的参数进行传递 */
    mrs r0, cpsr
    bl print_cpsr

    /* 6、写入一个未定义指令 */  
    .word 0xffffffff 

    /* 7、触发一个SVC异常 */
    SVC #1

    /* 8、允许中断触发，清除I位，使能中断*/
    CPSIE I  //

    /* 9、跳转到led函数 */
    bl main

    /* 10、原地循环 */
    b .

_undefined_instruction:
	/* 1、设置Undefined模式下的栈 */
	ldr sp, =(0x80000000+0x50000)
	
	/* 2、保存现场 */
	stmdb sp!, {r0-r12,lr}

	/* 3、调用处理函数 */
	bl do_undefined_c

	/* 4、恢复现场 */
	ldmia sp!, {r0-r12,pc}^

_software_interrupt:
    /* 1、设置SVC模式下的栈 */
	ldr sp, =(0x80000000+0x40000)
	
	/* 2、保存现场 */
	stmdb sp!, {r0-r12,lr}

	/* 3、调用处理函数 */
	bl do_svc_c

	/* 4、恢复现场 */
	ldmia sp!, {r0-r12,pc}^

_irq:
    /* 1、设置IRQ模式下的栈 */
	ldr sp, =(0x80000000+0x30000)
	
	/* 2、保存现场 */
    subs lr, lr, #4
	stmdb sp!, {r0-r12,lr}

	/* 3、调用处理函数 */
	bl do_irq_c

	/* 4、恢复现场 */
	ldmia sp!, {r0-r12,pc}^

