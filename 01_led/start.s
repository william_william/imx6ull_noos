
.text                               /* .text段保存代码，是只读和可执行的，后面的指令都属于.text段。 */
.global  _start                     /* .global表示_start是一个全局符号，会在链接器链接时用到 */

_start:                             /* 标签_start，汇编程序的默认入口是_start */
    /* 1、设置栈 */
    ldr sp, =(0x80000000+0x100000)  /* 设置栈顶地址 */
    /* 2、跳转到led函数 */
    bl main
    /* 3、原地循环 */
    b .
