
.text                               /* .text段保存代码，是只读和可执行的，后面的指令都属于.text段。 */
.global  _start                     /* .global表示_start是一个全局符号，会在链接器链接时用到 */

_start:                             /* 标签_start，汇编程序的默认入口是_start */
    /* 1、设置栈 */
    ldr sp, =(0x80000000+0x100000)  /* 设置栈顶地址 */

    /* 2、清除bss段 */
    ldr r1, =__bss_start            /* 将bss段开始地址存入r1寄存器 */
    ldr r2, =__bss_end              /* 将bss段结束地址存入r2寄存器 */
    b clean_bss
clean:
    mov r3, #0                      /* 将0存入r3寄存器 */
    str r3, [r1], #4                /* 将r3中的值存到r1中的值所指向的地址中, 同时r1中的值加4 */
clean_bss:
    cmp r1, r2                      /* 比较r1和r2内的值是否相等 */
    bne clean                       /* 如果不相等则跳转到clean标签处，如果相等则往下执行 */                      

    /* 3、跳转到led函数 */
    bl main

    /* 4、原地循环 */
    b .

