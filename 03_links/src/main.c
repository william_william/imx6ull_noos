#include "uart.h"
#include "led.h"

int mydata = 12315;
const int myconst = 22315;
int my[1024] = {0};
int myzero1 = 0;
int myzero2 = 0;

int main(void)
{
    uart_init();
    led_init();
    putstring("imx6ull\r\n");
    putstring("myzero1:");
    puthex((unsigned int)myzero1);
    putstring("\r\nmyzero2:");
    puthex((unsigned int)myzero2);
    putstring("\r\n");
    myzero1 = 0xdf133;
    myzero2 = 0x15647;

    while(1)
    {
        putstring("led on\r\n");
        led_on();
        delay(1000000);

        putstring("led off\r\n");
        led_off();
        delay(1000000);
    }
}
