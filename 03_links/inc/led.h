#ifndef __LED_H
#define __LED_H

int delay(int ndelay);
int led_init(void);
void led_on(void);
void led_off(void);

#endif
