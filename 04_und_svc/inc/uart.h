#ifndef _UART_H
#define _UART_H

void uart_init(void);
int getchar(void);
int	putchar(char c);
int putstring(const char *s);
void puthex(unsigned int val);

#endif