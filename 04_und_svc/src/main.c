#include "uart.h"
#include "led.h"

void SystemInit(void)
{
    uart_init();
}

int main(void)
{
    led_init();
    putstring("imx6ull\r\n");

    while(1)
    {
        putstring("led on\r\n");
        led_on();
        delay(1000000);

        putstring("led off\r\n");
        led_off();
        delay(1000000);
    }
}

void do_undefined_c(void)
{
	putstring("Exception: undefined instruction.\r\n");
}

void do_svc_c(void)
{
	putstring("Exception: software interrupt.\r\n");
}

void print_cpsr(unsigned int cpsr_val)
{
    putstring("Entered on reset CPSR's value: ");
    puthex(cpsr_val);
    putstring("\r\n");
}