# imx6ull_noos

#### 介绍
存放IMX6ULL的裸机编程代码，使用正点原子阿尔法开发板

简易的命令行入门教程:
Git 全局设置:
```bash
git config --global user.name "william"
git config --global user.email "472853046@qq.com"
```

创建 git 仓库:
```bash
mkdir GD32
cd GD32
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://gitee.com/william_william/imx6ull_noos.git
git push -u origin master
```
同步仓库:
```bash
git pull origin master